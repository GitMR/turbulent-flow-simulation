#include "VTKStencil.h"


VTKStencil::VTKStencil ( const Parameters & parameters ) :
    FieldStencil<FlowField> ( parameters ) {}


void VTKStencil::apply ( FlowField & flowField, int i, int j ) {
    // i varies between 1 and flowField.getNx()+1 (included)
    // j varies between 1 and flowField.getNy()+1 (included)

    // The first indices (i=1, j=1) correspond to ghost cells, which we
    // skip
    if (i > 1 && j > 1) {

	const int obstacle = flowField.getFlags().getValue(i, j);

	FLOAT velocity[2];
	FLOAT pressure;
	// Needed to interpolate the velocity
	FLOAT velocity_X_UPWIND, velocity_Y_UPWIND;

	flowField.getPressureAndVelocity(pressure, velocity, i, j);
	velocity_X_UPWIND = flowField.getVelocity().getVector(i-1, j)[0];
	velocity_Y_UPWIND = flowField.getVelocity().getVector(i, j-1)[1];

	velocity[0] += velocity_X_UPWIND;
	velocity[1] += velocity_Y_UPWIND;

	if (0 == (obstacle & OBSTACLE_SELF)) {
	    _velocity_str << velocity[0]/2 << " "
			  << velocity[1]/2 << " "
			  << "0" << std::endl;
	    _pressure_str << pressure << std::endl;
	}
	else {
	    _velocity_str << "0.0 0.0 0.0" << std::endl;
	    _pressure_str << "0.0" << std::endl;
	}
    }
}


void VTKStencil::apply ( FlowField & flowField, int i, int j, int k ) {
    // i varies between 1 and flowField.getNx()+1 (included)
    // j varies between 1 and flowField.getNy()+1 (included)
    // k varies between 1 and flowField.getNz()+1 (included)

    // The first indices (i=1, j=1, k=1) correspond to ghost cells, which we
    // skip
    if (i > 1 && j > 1 && k > 1) {

	const int obstacle = flowField.getFlags().getValue(i, j, k);

	FLOAT velocity[3];
	FLOAT pressure;
	// Needed to interpolate the velocity
	FLOAT velocity_X_UPWIND, velocity_Y_UPWIND, velocity_Z_UPWIND;

	flowField.getPressureAndVelocity(pressure, velocity, i, j, k);
	velocity_X_UPWIND = flowField.getVelocity().getVector(i-1, j, k)[0];
	velocity_Y_UPWIND = flowField.getVelocity().getVector(i, j-1, k)[1];
	velocity_Z_UPWIND = flowField.getVelocity().getVector(i, j, k-1)[2];

	velocity[0] += velocity_X_UPWIND;
	velocity[1] += velocity_Y_UPWIND;
	velocity[2] += velocity_Z_UPWIND;

	if (0 == (obstacle & OBSTACLE_SELF)) {
	    _velocity_str << velocity[0]/2 << " "
			  << velocity[1]/2 << " "
			  << velocity[2]/2 << std::endl;
	    _pressure_str << pressure << std::endl;
	}
	else {
	    _velocity_str << "0.0 0.0 0.0" << std::endl;
	    _pressure_str << "0.0" << std::endl;
	}
    }
}


void VTKStencil::write ( FlowField & flowField, int timeStep ) {

    // Generating filename
    std::stringstream filename;
    filename << _parameters.vtk.prefix << "_" << timeStep << ".vtk";

    // Opening the file
    _outfile.open(filename.str().c_str(), std::ios_base::out);

    _outfile << "# vtk DataFile Version 2.0" << std::endl
	     << _parameters.vtk.prefix << " [simulation at time step number "
	     << timeStep << "]"<< std::endl
	     << "ASCII" << std::endl << std::endl;

    // Writing down the vertices coordinates


    // TODO implement 2D case

    if (2 == _parameters.geometry.dim) {

	// Number of "non-ghost" points defining the staggered grid
	int nX, nY;
	nX = flowField.getNx()+1; // getNx() ==> number of non-ghost cells in X-direction
	nY = flowField.getNy()+1; // getNy() ==> number of non-ghost cells in Y-direction

	_outfile << "DATASET STRUCTURED_GRID" << std::endl
		 << "DIMENSIONS " << nX << " " << nY << " " << 1
		 << std::endl
		 << "POINTS " << nX * nY << " float" << std::endl;
	
	// We loop over the inner cells
	int i, j;
	i = j = 0;
	for (j = 2 ; j < nY+2 ; ++j) {
	    for (i = 2 ; i < nX+2 ; ++i) {
		_outfile << _parameters.meshsize->getPosX(i, j) << " "
			 << _parameters.meshsize->getPosY(i, j) << " "
			 << 0 << std::endl;
	    }
	}

	_outfile << std::endl;


	// Writing the pressure
	_outfile << "CELL_DATA " << (nX-1) * (nY-1) << std::endl
		 << "SCALARS pressure float 1" << std::endl
		 << "LOOKUP_TABLE default" << std::endl;
	_outfile << _pressure_str.str().c_str() << std::endl;
	_pressure_str.str(std::string());

	// Writing the velocity
	_outfile << "VECTORS velocity float" << std::endl;
	_outfile << _velocity_str.str().c_str() << std::endl;
	_velocity_str.str(std::string());

    }

    else {

	// Number of "non-ghost" points defining the staggered grid
	int nX, nY, nZ;
	nX = flowField.getNx()+1; // getNx() ==> number of non-ghost cells in X-direction
	nY = flowField.getNy()+1; // getNy() ==> number of non-ghost cells in Y-direction
	nZ = flowField.getNz()+1; // getNz() ==> number of non-ghost cells in Z-direction

	_outfile << "DATASET STRUCTURED_GRID" << std::endl
		 << "DIMENSIONS " << nX << " " << nY << " " << nZ
		 << std::endl
		 << "POINTS " << nX * nY * nZ << " float" << std::endl;
	
	// We loop over the inner cells
	int i, j, k;
	i = j = k = 0;
	for (k = 2 ; k < nZ+2 ; ++k) {
	    for (j = 2 ; j < nY+2 ; ++j) {
		for (i = 2 ; i < nX+2 ; ++i) {
		    _outfile << _parameters.meshsize->getPosX(i, j, k) << " "
			     << _parameters.meshsize->getPosY(i, j, k) << " "
			     << _parameters.meshsize->getPosZ(i, j, k)
			     << std::endl;
		}
	    }
	}

	_outfile << std::endl;


	// Writing the pressure
	_outfile << "CELL_DATA " << (nX-1) * (nY-1) * (nZ-1) << std::endl
		 << "SCALARS pressure float 1" << std::endl
		 << "LOOKUP_TABLE default" << std::endl;
	_outfile << _pressure_str.str().c_str() << std::endl;
	_pressure_str.str(std::string());

	// Writing the velocity
	_outfile << "VECTORS velocity float" << std::endl;
	_outfile << _velocity_str.str().c_str() << std::endl;
	_velocity_str.str(std::string());

    }


    // Closing the file
    _outfile.close();
}
