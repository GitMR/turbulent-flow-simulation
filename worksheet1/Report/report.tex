\documentclass[11pt,a4paper,DIV12,pdftex]{scrartcl}
\usepackage[natbibapa]{apacite}
\usepackage[english]{babel}
% \usepackage{geometry}
\usepackage{parskip}
\usepackage{float}
\usepackage{subfig}
\usepackage{mathpazo}
% \usepackage{lmodern}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{xcolor}
\usepackage{ifthen}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage[utf8]{inputenc}
\usepackage[dvips]{epsfig}
\usepackage{tikz}

\linespread{0.9}

\definecolor{blauTUM}{RGB}{0,101,189}

\titlehead{
   \includegraphics[height=1cm]{Universitaet_Logo_RGB.pdf} \\ \\
    Turbulent Flow Simulation on HPC-Systems \\
    Winter 2018/19 \\
    Technische Universit\"at M\"unchen 
}

\title{Worksheet 1: Getting Started}
\date{\vspace*{-2cm}}

\begin{document}



\maketitle



\begin{center}{Mathieu Rigal\smallbreak\today}\end{center}

\section{Flow physics and profiling}
This first worksheet deals with the implementation of a VTK-output regarding the
NS-EOF program, followed by a hands-on aiming at understanding the physical
behavior of different types of flows.

\subsection{Influence of the parameters}
In this section we investigate the influence of various parameters such as the
Reynolds number, the length and height of the backward-facing step and the used
mesh through several scenarios. These scenarios include a driven cavity flow in
two dimensions and a channel flow problem in three dimensions. The latter will
be considered with both a simple rectangular geometry and a backward-facing
step, which is a sudden increase of cross-sectional area (see fig.
\ref{channel-geometry}).
\begin{figure}[!htb]\centering
  \begin{tikzpicture}
    \draw[blauTUM,very thick,fill=cyan!5]
    (0,.2) -- (0,1) -- (5,1) -- (5,0) -- (1,0) -- (1,.2) -- (0,.2);
    \draw[blauTUM,thick,-latex] (-.8,.45) -- (-.1,.45);
    \draw[blauTUM,thick,-latex] (-.8,.75) -- (-.1,.75);
    \draw[blauTUM,thick,-latex] (5.1,.2) -- (5.8,.2);
    \draw[blauTUM,thick,-latex] (5.1,.4) -- (5.8,.4);
    \draw[blauTUM,thick,-latex] (5.1,.6) -- (5.8,.6);
    \draw[blauTUM,thick,-latex] (5.1,.8) -- (5.8,.8);
    \draw[gray,<-]
    (.5,.05) -- (.5,-.4) node[below] {\small\color{gray}backward-facing step};
  \end{tikzpicture}
  \caption{Sectional view of the channel geometry including a backward-facing
    step.}
  \label{channel-geometry}
\end{figure}

\subsubsection{The Reynolds number}
The Reynolds number $\mathrm{Re}$ characterizes the contribution of the inertial
forces in comparison to the viscous ones. If we denote $V$ the velocity of the
fluid,  $L$ the characteristic length and $\nu$ the kinematic viscosity of the
fluid, then the Reynolds number can be expressed as:
\begin{align}
  \mathrm{Re} = \frac{VL}{\nu}
\end{align}
If $\mathrm{Re}$ is small, the flow is considered to be \emph{laminar}, which
means that the fluid flows in parallel layers with no disruption between the
layers. On the contrary, if $\mathrm{Re}$ is big enough  the flow is called
\emph{turbulent}, which means that the pressure and velocity evolve in a chaotic
way.

\smallbreak
\begin{figure}[!htb]\centering
  \includegraphics[width=.495\textwidth]%
  {figures/cavity_flow_100x100_Re35_T30.jpg}\hfill
  \includegraphics[width=.495\textwidth]%
  {figures/cavity_flow_100x100_Re400_T30.jpg}\\
  \includegraphics[width=.495\textwidth]%
  {figures/cavity_flow_100x100_Re2000_T30.jpg}\hfill
  \includegraphics[width=.495\textwidth]%
  {figures/cavity_flow_100x100_Re4000_T30.jpg}\\
  \includegraphics[width=.495\textwidth]%
  {figures/cavity_flow_100x100_Re16000_T30.jpg}\hfill
  \includegraphics[width=.495\textwidth]%
  {figures/cavity_flow_100x100_Re32000_T30.jpg}
  \caption{Velocity of the cavity flow at time 30 for different Reynolds
    numbers: top-left $\mathrm{Re}=35$, top-right $\mathrm{Re}=400$,
    middle-left $\mathrm{Re}=2000$, middle-right $\mathrm{Re}=4000$,
    bottom-left: $\mathrm{Re}=16000$, bottom-right: $\mathrm{Re}=32000$.}
  \label{reynolds-impact-cavity}
\end{figure}
We first check whereas the NS-EOF program outputs a "chaotic" solution for high
Reynolds numbers and a "laminar" one for small values. Especially we expect
the driven cavity flow problem to exhibit more vortices for large
Reynolds numbers than for low ones. The configuration used through this
simulation is described below:
\begin{itemize}
\item Geometry: length $=$ width $= 1$;
\item Number of cells: $N_x = 100, N_y = 100$;
\item Time: 30;
\end{itemize}
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/cavity_flow_100x100_Re_PressureComparison_T30.jpg}
  \caption{Pressure of the cavity flow at time 30 for different Reynolds
    numbers: top-left $\mathrm{Re}=35$, top-right $\mathrm{Re}=400$,
    middle-left $\mathrm{Re}=2000$, middle-right $\mathrm{Re}=4000$,
    bottom-left: $\mathrm{Re}=16000$, bottom-right: $\mathrm{Re}=32000$.}
  \label{reynolds-impact-cavity-pressure}
\end{figure}
The results are plotted in figure \ref{reynolds-impact-cavity}. For
$\mathrm{Re}=35$, only one big vortex is emerging between the top and the center
of the domain. For higher Reynolds secondary vortices appear in the vicinity of
the lower corners, while the primary vortex shifts more towards the center of
the domain. As expected, the apparition of additional vortices is characteristic
of the turbulent aspect of the flow. Besides, the velocity magnitude decreases
in the neighborhood of the main vortex when the flow becomes more turbulent. In
fig. \ref{reynolds-impact-cavity-pressure} we see that a low pressure peak
(resp. a high pressure peak) appears at the upper left corner (resp. the upper
right corner).


\smallbreak
\begin{figure}[!htb]\centering
  \includegraphics[width=.495\textwidth]%
  {figures/channel_flow_ReInfluence_velocity_10x20x10_Re40_T10_rect.jpg}\hfill
  \includegraphics[width=.495\textwidth]%
  {figures/channel_flow_ReInfluence_velocity_10x20x10_Re400_T10_rect.jpg}\\
  \includegraphics[width=.495\textwidth]%
  {figures/channel_flow_ReInfluence_velocity_10x20x10_Re4000_T10_rect.jpg}\hfill
  \includegraphics[width=.495\textwidth]%
  {figures/channel_flow_ReInfluence_velocity_10x20x10_Re16000_T10_rect.jpg}\\
  \caption{%
    Velocity magnitude of the channel flow at time 1.33 for different Reynolds
    numbers:
    top-left $\mathrm{Re}=40$,
    top-right $\mathrm{Re}=400$,
    bottom-left $\mathrm{Re}=4000$,
    bottom-right $\mathrm{Re}=16000$.}
  \label{reynolds-impact-on-velocity-channel}
\end{figure}
% \begin{figure}[h]\centering
%   \includegraphics[width=.495\textwidth]%
%   {figures/channel_flow_ReInfluence_pressure_10x20x10_Re40_T10_rect.jpg}\hfill
%   \includegraphics[width=.495\textwidth]%
%   {figures/channel_flow_ReInfluence_pressure_10x20x10_Re400_T10_rect.jpg}\\
%   \includegraphics[width=.495\textwidth]%
%   {figures/channel_flow_ReInfluence_pressure_10x20x10_Re4000_T10_rect.jpg}\hfill
%   \includegraphics[width=.495\textwidth]%
%   {figures/channel_flow_ReInfluence_pressure_10x20x10_Re16000_T10_rect.jpg}\\
%   \caption{%
%     Pressure field of the channel flow at time 1.33 for different Reynolds
%     numbers:
%     top-left $\mathrm{Re}=40$,
%     top-right $\mathrm{Re}=400$,
%     bottom-left $\mathrm{Re}=4000$,
%     bottom-right $\mathrm{Re}=16000$}
%   \label{reynolds-impact-on-pressure-channel}
% \end{figure}
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/channel_Re40_pressure_profile_T10_rect.jpg}
  \includegraphics[width=\textwidth]%
  {figures/channel_Re400_pressure_profile_T10_rect.jpg}% \\
  % \includegraphics[width=\textwidth]%
  % {figures/channel_Re4000_pressure_profile_T10_rect.jpg}% //
  % \includegraphics[width=\textwidth]%
  % {figures/channel_Re16000_pressure_profile_T10_rect.jpg}
  % \caption{%
  %   Left: pressure field of the channel flow at iteration 1 for different Reynolds
  %   numbers (from top to bottom: $\mathrm{Re}=40$, $\mathrm{Re}=400$,
  %   $\mathrm{Re}=4000$, $\mathrm{Re}=16000$). Right: associated profile.}
  % \label{reynolds-impact-on-pressure-channel}
\end{figure}
\begin{figure}[!htb]\ContinuedFloat\centering
  \includegraphics[width=\textwidth]%
  {figures/channel_Re4000_pressure_profile_T10_rect.jpg}
  \includegraphics[width=\textwidth]%
  {figures/channel_Re16000_pressure_profile_T10_rect.jpg}
  \caption{%
    Left: pressure field of the channel flow at iteration 1 for different Reynolds
    numbers (from top to bottom: $\mathrm{Re}=40$, $\mathrm{Re}=400$,
    $\mathrm{Re}=4000$, $\mathrm{Re}=16000$). Right: associated profile.}
  \label{reynolds-impact-on-pressure-channel}
\end{figure}
\newpage
The decrease of velocity is also encountered in the case of the channel
flow with a simple rectangular geometry. Figure
\ref{reynolds-impact-on-velocity-channel} shows this for four different Reynolds
numbers. Again, we see that the velocity magnitude decreases when the
Reynolds number increases. As for the pressure, it drops linearly throughout the
whole domain. In addition, we see that the maximum pressure is impacted by the
Reynolds number, which can be seen in fig.
\ref{reynolds-impact-on-pressure-channel}. Note that the color-map had to be
re-scaled each time to see the pressure variation in space.

\smallbreak
% \begin{figure}[h]\centering
%   \includegraphics[width=\textwidth]%
%   {figures/channel_Re100_velocity_pressure_iter0.jpg}
%   \caption{%
%     Channel flow with backward-facing step at iteration 0 for different Reynolds
%     numbers (from top to bottom: $\mathrm{Re}=100$, $\mathrm{Re}=8000$,
%     $\mathrm{Re}=24000$).}
%   \label{reynolds-impact-on-pressure-channel-backward-facing-step-iter00}
% \end{figure}
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/channel_Re100_velocity_pressure_iter15.jpg}
  \caption{%
    Channel flow with backward-facing step at iteration 15 for different Reynolds
    numbers (from top to bottom: $\mathrm{Re}=100$, $\mathrm{Re}=8000$,
    $\mathrm{Re}=24000$). Right: velocity magnitude. Left: pressure field.}
  \label{reynolds-impact-on-pressure-channel-backward-facing-step-iter15}
\end{figure}
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/channel_Re100_velocity_pressure_iter98.jpg}
  \caption{%
    Channel flow with backward-facing step at iteration 98 for different Reynolds
    numbers (from top to bottom: $\mathrm{Re}=100$, $\mathrm{Re}=8000$,
    $\mathrm{Re}=24000$). Right: velocity magnitude. Left: pressure field.}
  \label{reynolds-impact-on-pressure-channel-backward-facing-step-iter98}
\end{figure}
Finally, we consider the velocity/pressure field behind the backward-facing step
(see fig. \ref{reynolds-impact-on-pressure-channel-backward-facing-step-iter15}
and \ref{reynolds-impact-on-pressure-channel-backward-facing-step-iter98}).
As for the cavity scenario, a primary vortex appears just at the bottom right of
the step. For low Reynolds numbers, this vortex doesn't shift towards the right,
and the area where the velocity magnitude is high stays in the narrow section
(before the step). For higher Reynolds numbers, both the vortex and the high
velocity area expand towards the flow direction (see fig.
\ref{reynolds-impact-on-pressure-channel-backward-facing-step-iter98}).
Similarly to the channel flow with a simple rectangular geometry, the pressure
behind the step gets lower when $\mathrm{Re}$ is incremented.


\subsubsection{Length and height of the backward-facing step}
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/width_backward_facing_step_20x40x10_iter00_picture.jpg}
  \caption{Comparison of several widths for the backward-facing step at
    iteration 1 ($\mathrm{Re}=4000$). Left: velocity magnitude, right: pressure
    field. The height ratio of the backward-facing step is $0.8$, whereas the
    width ratio ranges from $0.6$ to $0.2$ with decrements of $0.2$.}
  \label{width-backward-facing-step-comparison-iter00}
\end{figure}
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/width_backward_facing_step_20x40x10_iter66_picture.jpg}
  \caption{Same as in figure \ref{width-backward-facing-step-comparison-iter00}
    for iteration 66.}
  \label{width-backward-facing-step-comparison-iter66}
\end{figure}
When the backward-facing step has an important width ($x$ direction), the fluid
flows naturally faster over a long distance in the narrow part of the channel,
% before slowing down at the step.
and is thus ahead of a fluid that would go through a shorter backward-facing
step. This behavior can be assessed through figures
\ref{width-backward-facing-step-comparison-iter00} and
\ref{width-backward-facing-step-comparison-iter66} which show a sectional view
of a $20\times 40\times 10$ 3D grid at different time steps. We also display the
streamlines and the pressure. We can see that there is a significant pressure
drop at channel opening for widths $0.4$ and $0.6$ in fig.
\ref{width-backward-facing-step-comparison-iter00}. Finally the displayed
streamlines emphasize the creation of a primary vortex at lower right of step for
widths $0.2$ and $0.4$ (fig. \ref{width-backward-facing-step-comparison-iter66}).
If the backward-facing step's width is too big, there is no vortex.

\smallbreak
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/height_backward_facing_step_20x40x10_iter26_picture.jpg}
  \caption{Comparison of several heights for the backward-facing step at
    iteration 26 ($\mathrm{Re}=4000$). Left: velocity magnitude, right: pressure
    field. The width ratio of the backward-facing step is $0.2$, whereas the
    height ratio ranges from $0.4$ to $0.8$ with increments of $0.2$.}
  \label{backward-facing-step-comparison-iter26}
\end{figure}
\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/height_backward_facing_step_20x40x10_iter52_picture.jpg}
  \caption{Same as in figure \ref{backward-facing-step-comparison-iter26}
    for iteration 52.}
  \label{backward-facing-step-comparison-iter52}
\end{figure}
The height's ratio influences only a little the velocity. Larger sections before
the step (i.e. smaller heights of the step) allow the fluid to loose less
speed while reaching the step, although this is barely noticeable.
% Behind the step the change is more important for
% the pressure, which is greater for higher heights of the step (see fig.
% \ref{backward-facing-step-comparison-iter26}).
Behind the step the pressure increases as soon as the step gets higher
(see fig. \ref{backward-facing-step-comparison-iter26} and
\ref{backward-facing-step-comparison-iter52}).

% \begin{figure}[h]\centering
%   \includegraphics[width=.495\textwidth]%
%   {figures/small_backward_facing_step_picture.jpg}\hfill
%   \includegraphics[width=.495\textwidth]%
%   {figures/small_backward_facing_step_plot.jpg}
%   \caption{Big backward step}
%   \label{small-backward}
% \end{figure}
% \begin{figure}[h]\centering
%   \includegraphics[width=.495\textwidth]%
%   {figures/big_backward_facing_step_picture.jpg}\hfill
%   \includegraphics[width=.495\textwidth]%
%   {figures/big_backward_facing_step_plot.jpg}
%   \caption{Big backward step}
%   \label{big-backward}
% \end{figure}

\newpage
\subsubsection{Mesh size and type}
At last we investigate the mesh size and type influence on the overall accuracy
and time step size. For this we run several simulations with different meshes
of the channel with rectangular geometry (no backward-facing step). The used
sizes are listed below:
\begin{itemize}
\item $10\times20\times10$, uniform and stretched;
\item $15\times30\times10$, uniform and stretched;
\item $20\times40\times10$, uniform and stretched;
\item $20\times40\times20$, uniform only;
\end{itemize}

The generated figures \ref{mesh_influence_velocity} and
\ref{mesh_influence_pressure} are sectional views orthogonal to
the flow direction at position $x=2.5$.

\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/mesh_influence_velocity1.jpg}
  \caption{Velocity magnitude at iteration 30 ($Re=24000$).
    Top: uniform mesh.
    Bottom: stretched mesh,.
    Sizes from left to right:
    $10\times20\times10,\
    15\times30\times10,\
    20\times40\times10,\
    20\times40\times20$.} 
  \label{mesh_influence_velocity}
\end{figure}

\begin{figure}[!htb]\centering
  \includegraphics[width=\textwidth]%
  {figures/mesh_influence_pressure1.jpg}
  \caption{Pressure field at iteration 30 ($Re=24000$).
    Top: uniform mesh.
    Bottom: stretched mesh,.
    Sizes from left to right:
    $10\times20\times10,\
    15\times30\times10,\
    20\times40\times10,\
    20\times40\times20$.} 
  \label{mesh_influence_pressure}
\end{figure}

Even for coarse meshes, the velocity magnitude seems to be accurately
approximated since it doesn't change much in comparison to finer meshes
(see fig. \ref{mesh_influence_velocity}). For a given size, using a stretched
mesh or a uniform one is not really impacting the result.

\smallbreak
\begin{table}[!htb]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    & \multicolumn{2}{c|}{\bfseries Time step} \\
    \hline
    \itshape Size & \itshape Uniform mesh & \itshape Stretched mesh \\
    \hline
    $10\times20\times10$ & 0.112515 & 0.0548318 \\
    \hline
    $15\times30\times10$ & 0.0749055 & 0.0217969 \\
    \hline
    $20\times40\times10$ & 0.0561518 & 0.0114323 \\
    \hline
    $20\times40\times20$ & 0.0557296 & \\
    \hline
  \end{tabular}
  \caption{Time step for different meshes.}
  \label{mesh-influence-over-time-step}
\end{table}
The situation is different for the pressure field. The result associated to  the
uniform mesh is quite impacted by the grid size: even the shape of the high
pressure area is changing (see fig. \ref{mesh_influence_pressure}, in red). For
the stretched mesh this is better, although the pressure range unfortunately
changes depending on the grid size --- which is also the case for the uniform
mesh. For a pressure-accurate simulation, one should thus preferably use a
stretched mesh with as many points as possible.

\smallbreak
The average time step is given for each used mesh in table
\ref{mesh-influence-over-time-step}. As expected the time step decreases when
the number of points increases. The decrease is slower for the uniform mesh, and
we see that the time step has to be much smaller for a stretched mesh than for a
similarly sized uniform mesh. This is due to the stability condition:
\begin{align}
  \Delta t < \min \bigg\{
  \frac{\mathrm{Re}}{2} \bigg(\frac{1}{\Delta x^2} + \frac{1}{\Delta y^2} +
  \frac{1}{\Delta z^2}\bigg)^{-1},\
  \frac{\Delta x}{\vert u_\text{max}\vert},\
  \frac{\Delta y}{\vert v_\text{max}\vert},\
  \frac{\Delta z}{\vert w_\text{max}\vert}
  \bigg\}
\end{align}
In conclusion, running the simulation on a stretched mesh requires more time
than a uniform mesh, but seems to be preferable to approximate the pressure
field with a better accuracy. Ultimately, finding a compromise
between the pressure field accuracy and the number of operations will be needed.


\subsection{Profiling}
\begin{table}[!htb]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    \bfseries Function name & \bfseries Self time (in s) & \bfseries Percentage \\
    \hline
    \ttfamily MatSolve\_SeqAIJ\_NaturalOrdering & 372.55 & 43.32 \\
    \hline
    \ttfamily MatMult\_SeqAIJ & 160.65 & 18.68 \\
    \hline
    \ttfamily PetscAbsScalar & 75.08 & 8.73 \\
    \hline
    \ttfamily VecMAXPY\_Seq & 68.67 & 7.98 \\
    \hline
    \vdots & \vdots & \vdots \\
    \hline
    \ttfamily VTKStencil::write(FlowField\&, int) & 0.03 & 0.00 \\
    \hline
  \end{tabular}
  \caption{Time spent by each function.}
  \label{profiler}
\end{table}
We want to determine the NS-EOF functions that are the most
time-consuming. To reach this goal the profiler \emph{gprof} is used.
The overall duration of the studied simulation is 860.00s. The statistics
outputted by \emph{gprof} include a flat profile and a call graph.
The table \ref{profiler} summarizes the flat profile by showing how much time
was consumed by some of the functions.

\smallbreak
We see that two functions are responsible of more than $60\%$ of the elapsed
time:
\begin{itemize}
\item {\ttfamily MatSolve\_SeqAIJ\_NaturalOrdering};
\item {\ttfamily MatMult\_SeqAIJ};
\end{itemize}
These two functions run in a sequential way, so a mean of improving the code
performance could be to parallelize them. The VTK-output has a negligible
effect.


% \bibliographystyle{apacite}
% \bibliography{literature}

\end{document}
