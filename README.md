# Turbulent Flow Simulation on HPC-systems
*Mathieu RIGAL*

## Overview
Finite difference scheme for solving the Navier-Stokes equation (pressure and
velocity) on a staggered grid. Handles 2D and 3D geometries including obstacles
(walls).
